package com.example.app;

import com.example.app.models.BankClient;
import com.example.app.models.AccountClient;
import com.example.app.models.TransActions;
import com.example.app.repositories.AccountClientRepository;
import com.example.app.repositories.BankClientRepository;
import com.example.app.repositories.TransActionsRepository;
import com.example.app.service.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(properties = {
        "command.line.runner.enabled=true",
        "application.runner.enabled=true"})
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.example.app")
@ActiveProfiles("test")
public class MiniBankTest {

    @Autowired
    BankClientRepository bankClientRepository;

    @Autowired
    public AccountClientRepository accountClientRepository;

    @Autowired
    public TransActionsRepository transActionsRepository;

    @Autowired
    BankClientService bankClientService;

    @Autowired
    AccountClientService accountClientService;

    @Autowired
    TransActionsService transActionsService;

    private BankClient bankClient1 = new BankClient();
    private BankClient bankClient2 = new BankClient();
    private AccountClient accountClient1 = new AccountClient();
    private AccountClient accountClient2 = new AccountClient();
    private TransActions transActions = new TransActions();

    private final static String OPEN_STATUS = "Open";

    @Before
    public void testBefore() {
        bankClient1.setStatus(OPEN_STATUS);
        bankClient1.setName("TestOrg1");
        bankClient1 = bankClientService.openBankClient("TestOrg1");
        bankClient2.setStatus(OPEN_STATUS);
        bankClient2.setName("TestOrg2");
        bankClient2 = bankClientService.openBankClient("TestOrg2");
        accountClient1.setName("Acco1");
        accountClient1.setStatus(OPEN_STATUS);
        accountClient1.setBalance(100.0);
        accountClient1.setBankClient(bankClient1);
        accountClient1 = accountClientService.createNewAccount("TestOrg1","Acco1",100.0);
        accountClient2.setName("Acco2");
        accountClient2.setStatus(OPEN_STATUS);
        accountClient2.setBalance(200.0);
        accountClient2.setBankClient(bankClient2);
        accountClient2 = accountClientService.createNewAccount("TestOrg2","Acco2",200.0);
        transActions.setSumma(100.0);
        transActions.setCredit(accountClient1);
        transActions.setDebet(accountClient2);
        transActions = transActionsService.createTransActions("Acco1","Acco2",50.0);
    }

    @Test
    public void createClientT() {

        assertEquals("Проверка количества новых клиентов", bankClientRepository.count(), 2L);
        assertEquals("Проверка наименования клиента", bankClientService
                .checkClient("TestOrg1"), OPEN_STATUS);
        assertEquals("Проверка наименования клиента", bankClientService
                .checkClient("TestOrg2"), OPEN_STATUS);
        assertNotEquals("Проверка наименования клиента", bankClientService.checkClient("TestOrg2"), "Closed");
        assertEquals("Проверка метода возвращающего клиента",
                bankClientService.getClient("TestOrg1", OPEN_STATUS, 1).getName(), "TestOrg1");
        assertEquals("Проверка метода возвращающего клиента",
                bankClientService.getClient("TestOrg2", OPEN_STATUS, 2).getName(), "TestOrg2");
        assertNotEquals("Проверка метода возвращающего клиента",
                bankClientService.getClient("TestOrg2", OPEN_STATUS, 2), bankClient1);
        assertEquals("Проверка метода возвращающего айдишник клиента",
                bankClientService.getClientId("TestOrg1"), 1);
        assertEquals("Проверка метода возвращающего айдишник клиента",
                bankClientService.getClientId("TestOrg2"), 2);
        assertNotEquals("Проверка метода возвращающего айдишник клиента",
                bankClientService.getClientId("TestOrg2"), 1);
        assertEquals("Проверка метода возвращающего наименование клиента",
                bankClientService.getClientNameById(1), "TestOrg1");
        assertEquals("Проверка метода возвращающего наименование клиента",
                bankClientService.getClientNameById(2), "TestOrg2");
        assertNotEquals("Проверка метода возвращающего наименование клиента",
                bankClientService.getClientNameById(2), "TestOrg1");
        assertEquals("Проверка количества новых счетов", accountClientService
                .getAllAccountByClient(bankClient1).size(), 1);
        assertEquals("Проверка айдишника владельца счета по наименованию счета",
                accountClientService.getClientByAccount("Acco1"), 1);
        assertEquals("Проверка айдишника владельца счета по наименованию счета",
                accountClientService.getClientByAccount("Acco2"), 2);
        assertNotEquals("Проверка айдишника владельца счета по наименованию счета",
                accountClientService.getClientByAccount("Acco2"), 1);
        assertEquals("Проверка айдишника счета по наименованию счета",
                accountClientService.getAccount("Acco1"), 3);
        assertEquals("Проверка айдишника счета по наименованию счета",
                accountClientService.getAccount("Acco2"), 4);
        assertNotEquals("Проверка айдишника счета по наименованию счета",
                accountClientService.getAccount("Acco2"), 1);
        assertEquals("Проверка баланса счета по наименованию счета",
                accountClientService.getAccountBalanceByName("Acco1"), 150.0);
        assertEquals("Проверка баланса счета по наименованию счета",
                accountClientService.getAccountBalanceByName("Acco2"), 150.0);
        assertNotEquals("Проверка баланса счета по наименованию счета",
                accountClientService.getAccountBalanceByName("Acco2"), 100.0);
        assertEquals("Проверка счета клиента по его айдишнику",
                accountClientService.getAccountClient(3).getId(), accountClient1.getId());
        assertEquals("Проверка счета клиента по его айдишнику",
                accountClientService.getAccountClient(4).getId(), accountClient2.getId());
        assertNotEquals("Проверка счета клиента по его айдишнику",
                accountClientService.getAccountClient(3), accountClient2);
        assertEquals("Проверка списка счетов по владельцу",
                accountClientService.getAllAccountByClient(bankClient1).size(), 1);
        assertEquals("Проверка списка счетов по владельцу",
                accountClientService.getAllAccountByClient(bankClient2).size(), 1);
        assertNotEquals("Проверка списка счетов по владельцу",
                accountClientService.getAllAccountByClient(bankClient1).size(), 2);
        assertEquals("Проверка количества транзакций",
                transActionsRepository.count(), 1L);
    }

    @After
    public void afterTest() {
        bankClient1 = null;
        bankClient2 = null;
        accountClient1 = null;
        accountClient2 = null;
        transActions = null;
    }
}
