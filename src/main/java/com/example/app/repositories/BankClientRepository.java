package com.example.app.repositories;

import com.example.app.models.BankClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Репозиторий для работы с клиентами банка BankClient
 */
@Repository
public interface BankClientRepository extends JpaRepository<BankClient, Integer> {

    BankClient findBankClientByName(String name);

}
