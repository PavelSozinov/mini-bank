package com.example.app.repositories;

import com.example.app.models.AccountClient;
import com.example.app.models.BankClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий для работы с классом счетов клиентов AccountClient
 */
@Repository
public interface AccountClientRepository extends JpaRepository<AccountClient, Integer> {

    List<AccountClient> findAllAccountByBankClient(BankClient bankClient);

    AccountClient findAccountClientByName(String accountName);
}