package com.example.app.repositories;

import com.example.app.models.AccountClient;
import com.example.app.models.TransActions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий для работы с классом TransActions
 */
@Repository
public interface TransActionsRepository extends JpaRepository<TransActions, Integer> {

    List<TransActions> findAllTransActionsByDebetOrCredit(AccountClient debet, AccountClient credit);
}
