package com.example.app.service;

import com.example.app.models.TransActions;
import com.example.app.models.AccountClient;
import com.example.app.repositories.AccountClientRepository;
import com.example.app.repositories.BankClientRepository;
import com.example.app.repositories.TransActionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Сервисы для класса TransActions
 */
@Service
@Transactional
public class TransActionsServiceImpl implements TransActionsService {

    @Autowired
    private TransActionsRepository transActionsRepository;
    @Autowired
    private BankClientRepository bankClientRepository;
    @Autowired
    private AccountClientRepository accountClientRepository;

    @Autowired
    private AccountClientService accountClientService;

    private final static String CLOSED_STATUS = "Closed";
    private final static String OPEN_STATUS = "Open";

    public TransActionsServiceImpl(TransActionsRepository transActionsRepository) {
        this.transActionsRepository = transActionsRepository;
    }

    @Override
    public void printTransActions(String accountName) {
        List<TransActions> transActionsList = transActionsRepository
                .findAllTransActionsByDebetOrCredit
                        (accountClientService.getAccountClient(accountClientService.getAccount(accountName)),
                                accountClientService.getAccountClient(accountClientService.getAccount(accountName)));
        System.out.println("Счет кредита " + "Счет дебета " + "Сумма транзакции");
        transActionsList.stream().map(transActions -> transActions.getCredit().getName().concat("       ") + " "
                + transActions.getDebet().getName().concat("      ") + " "
                + transActions.getSumma()).forEach(System.out::println);
    }

    @Override
    public TransActions createTransActions(String accountDebetName, String accountCreditName, Double summa) {
        TransActions transActions = new TransActions();
        AccountClient accountClient = accountClientService
                .getAccountClient(accountClientService
                        .getAccount(accountDebetName));
        accountClient.setBalance(accountClientService.getAccountBalanceByName(accountDebetName) + summa);
        accountClient.setStatus(OPEN_STATUS);
        accountClient.setName(accountDebetName);
        accountClient.setBankClient(bankClientRepository.getOne(accountClientService.getClientByAccount(accountDebetName)));
        accountClient = accountClientRepository.saveAndFlush(accountClient);
        transActions.setCredit(accountClient);
        accountClient = accountClientService.getAccountClient(accountClientService.getAccount(accountCreditName));
        accountClient.setBalance(accountClientService.getAccountBalanceByName(accountCreditName) - summa);
        accountClient.setName(accountCreditName);
        if ((accountClientService.getAccountBalanceByName(accountCreditName) - summa) == 0) {
            accountClient.setStatus(CLOSED_STATUS);
        } else accountClient.setStatus(OPEN_STATUS);
        accountClient.setBankClient(bankClientRepository.getOne(accountClientService.getClientByAccount(accountCreditName)));
        accountClient = accountClientRepository.saveAndFlush(accountClient);
        transActions.setDebet(accountClient);
        transActions.setSumma(summa);
        transActions = transActionsRepository.saveAndFlush(transActions);
        return transActions;
    }
}
