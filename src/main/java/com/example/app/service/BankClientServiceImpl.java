package com.example.app.service;

import com.example.app.models.AccountClient;
import com.example.app.models.BankClient;
import com.example.app.repositories.AccountClientRepository;
import com.example.app.repositories.BankClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервисы для класса BankClient
 */
@Service
@Transactional
public class BankClientServiceImpl implements BankClientService {

    @Autowired
    public BankClientRepository bankClientRepository;
    @Autowired
    public AccountClientRepository accountClientRepository;
    private final static String OPEN_STATUS = "Open";
    private final static String CLOSED_STATUS = "Closed";

    public BankClientServiceImpl(BankClientRepository bankClientRepository) {
        this.bankClientRepository = bankClientRepository;
    }

    @Override
    public String checkClient(String clientName) {
        BankClient bankClient = bankClientRepository.findBankClientByName(clientName);
        return bankClient.getStatus();
    }

    @Override
    public BankClient getClient(String clientName, String status, Integer id) {
        BankClient bankClient;
        if (getClientNameById(id).isEmpty()) {
            bankClient = new BankClient();
        } else {
            bankClient = bankClientRepository.getOne(id);
        }
        bankClient.setName(clientName);
        bankClient.setStatus(status);
        bankClient.setId(id);
        return bankClient;
    }

    @Override
    public String getClientNameById(Integer id) {
        BankClient bankClient = bankClientRepository.getOne(id);
        return bankClient.getName();
    }

    @Override
    public Integer getClientId(String clientName) {
        BankClient bankClient = bankClientRepository.findBankClientByName(clientName);
        return bankClient.getId();
    }

    @Override
    public void printClient() {
        System.out.println("Наименование клиента " + "Статус клиента");
        List<BankClient> bankClientRepositoryAll = bankClientRepository.findAll();
        bankClientRepositoryAll.stream()
                .map(bankClient -> bankClient.getName().concat("             ") + " "
                        + bankClient.getStatus())
                .forEach(System.out::println);
    }

    @Override
    public BankClient openBankClient(String clientName) {
        BankClient bankClient = null;
        List<BankClient> bankClientRepositoryAll = bankClientRepository.findAll();
        if (!bankClientRepositoryAll
                .stream().anyMatch((n) -> n.getName().equals(clientName))) {
            bankClient = new BankClient();
        } else {
            bankClient = bankClientRepository.getOne(getClientId(clientName));
        }
        bankClient.setName(clientName.trim().replaceAll("\\s+", ""));
        bankClient.setStatus(OPEN_STATUS);
        bankClient = bankClientRepository.save(bankClient);
        return bankClient;
    }

    @Override
    public void closeBankClient(String clientName) {
        BankClient bankClient = getClient(clientName, OPEN_STATUS, getClientId(clientName));
        bankClient.setStatus(CLOSED_STATUS);
        bankClientRepository.save(bankClient);
        try {
            List<AccountClient> allAccountsList2 = accountClientRepository.findAll()
                    .stream()
                    .filter((n) -> n.getBankClient().getId().equals(getClientId(clientName)))
                    .collect(Collectors.toList());
            allAccountsList2.stream()
                    .forEach((p) -> {
                        p.setStatus(CLOSED_STATUS);
                        p.setBalance(0.0);
                        p = accountClientRepository.saveAndFlush(p);
                    });
            allAccountsList2.stream()
                    .map(accountClient -> "Счет " + accountClient.getName().concat(" закрыт"))
                    .forEach(System.out::println);
        } catch (java.lang.NullPointerException exception) {
            System.out.println("У клиента " + clientName + " нет счетов");
        }
        System.out.println("Клиент " + clientName + " закрыт");
    }
}
