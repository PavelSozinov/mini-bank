package com.example.app.service;

import com.example.app.models.TransActions;

/**
 * Интерфейс для вызова сервисов TransActionsService для работы с классом TransActions
 */
public interface TransActionsService {

    /**
     * Метод выполняет печать транзакций по счету
     *
     * @param accountName - наименование счета
     */
    void printTransActions(String accountName);

    /**
     * Метод создает новую транзакцию
     *
     * @param accountDebetName  Наименование счета дебета
     * @param accountCreditName Наименование счета кредита
     * @param summa             Сумма транзакции
     * @return
     */
    TransActions createTransActions(String accountDebetName, String accountCreditName, Double summa);
}