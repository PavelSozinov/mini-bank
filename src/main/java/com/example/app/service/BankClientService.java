package com.example.app.service;

import com.example.app.models.BankClient;

/**
 * Интерфейс для вызова сервисов BankClientService для работы с классом BankClient
 */
public interface BankClientService {

    /**
     * Метод возвращает статус клиента по наименованию
     *
     * @param clientName Наименование клиента
     * @return Статус клиента
     */
    String checkClient(String clientName);

    /**
     * Метод возвращает айдишник клиента по наименованию клиента
     *
     * @param clientName Наименование клиента
     * @return
     */
    Integer getClientId(String clientName);

    /**
     * Метод возвращает клиента по параметрам:
     *
     * @param clientName Наименование клиента
     * @param status     Статус клиента
     * @param id         Айдишник клиента
     * @return Клиент
     */
    BankClient getClient(String clientName, String status, Integer id);

    /**
     * Метод возвращает наименование клиента по айдишнику
     *
     * @param id Айдишник клиента
     * @return Наименование клиента
     */
    String getClientNameById(Integer id);

    /**
     * Метод печатает клиентов и выводит их наименование и статус
     */
    void printClient();

    /**
     * Метод открывает нового клиента или позволяет открыть ранее закрытого клиента
     *
     * @param clientName Наименование клиента
     * @return Возвращает клиента
     */
    BankClient openBankClient(String clientName);

    /**
     * Метод закрывает клиента и его счета
     *
     * @param clientName Наименование клиента
     */
    void closeBankClient(String clientName);
}