package com.example.app.service;

import com.example.app.models.AccountClient;
import com.example.app.models.BankClient;
import com.example.app.repositories.AccountClientRepository;
import com.example.app.repositories.BankClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Сервис для методов работы с классом AccountClient
 */
@Service
@Transactional
public class AccountClientServiceImpl implements AccountClientService {

    @Autowired
    private AccountClientRepository accountClientRepository;
    @Autowired
    private BankClientRepository bankClientRepository;
    @Autowired
    private BankClientService bankClientService;

    private final static String CLOSED_STATUS = "Closed";
    private final static String OPEN_STATUS = "Open";

    public AccountClientServiceImpl(AccountClientRepository accountClientRepository) {
        this.accountClientRepository = accountClientRepository;
    }

    @Override
    public Double getAccountBalanceByName(String accountName) {
        AccountClient accountClient = accountClientRepository.findAccountClientByName(accountName);
        return accountClient.getBalance();
    }

    @Override
    public AccountClient getAccountClient(Integer accountClientId) {
        AccountClient accountClient = new AccountClient();
        accountClient.setId(accountClientId);
        return accountClient;
    }

    @Override
    public Integer getAccount(String accountName) {
        AccountClient accountClient = accountClientRepository.findAccountClientByName(accountName);
        return accountClient.getId();
    }

    @Override
    public List<AccountClient> getAllAccountByClient(BankClient bankClient) {
        return accountClientRepository.findAllAccountByBankClient(bankClient);
    }

    @Override
    public Integer getClientByAccount(String accountName) {
        AccountClient accountClient = accountClientRepository.findAccountClientByName(accountName);
        return accountClient.getBankClient().getId();
    }

    @Override
    public void closeAccount(String accountName) {
        List<AccountClient> accountClientList = accountClientRepository.findAll();
        accountClientList.stream().filter((n) -> n.getName().equals(accountName))
                .forEach((p) -> {
                    p.setStatus(CLOSED_STATUS);
                    p.setBalance(0.0);
                    p = accountClientRepository.saveAndFlush(p);
                });
        System.out.println("Счет " + accountName + " закрыт!!!");
    }

    @Override
    public AccountClient createNewAccount(String clientName, String accountName, Double startBalance) {
        AccountClient accountClient = new AccountClient();
        accountClient.setName(accountName.trim().replaceAll("\\s+", ""));
        accountClient.setStatus(OPEN_STATUS);
        accountClient.setBalance(startBalance);
        accountClient.setBankClient(bankClientService.getClient(clientName,
                OPEN_STATUS,
                bankClientService.getClientId(clientName)));
        accountClient = accountClientRepository.saveAndFlush(accountClient);
        return accountClient;
    }

    @Override
    public void printAccount(String clientName) {
        List<AccountClient> accountThisClientList = getAllAccountByClient(bankClientRepository.getOne(bankClientService.getClientId(clientName)));
        System.out.println("Наименование счета " + "Статус " + "Баланс");
        accountThisClientList
                .stream()
                .map(accountClient -> accountClient.getName().concat("             ") + " "
                        + accountClient.getStatus().concat("  ") + " "
                        + accountClient.getBalance().toString())
                .forEach(System.out::println);
    }
}
