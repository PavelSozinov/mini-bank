package com.example.app.service;

import com.example.app.models.AccountClient;
import com.example.app.models.BankClient;

import java.util.List;

/**
 * Интерфейс для работы с сервисами класса AccountClient
 */
public interface AccountClientService {

    /**
     * Метод возвращает баланс счета по его наименованию
     *
     * @param accountName Наименование счета
     * @return Баланс счета
     */
    Double getAccountBalanceByName(String accountName);

    /**
     * Метод возвращает счет клиента по его айдишники
     *
     * @param accountClientId Айдишник счета
     * @return Возвращает счет клиента
     */
    AccountClient getAccountClient(Integer accountClientId);

    /**
     * Метод возвращает айдишник владельца счета по наименованию
     *
     * @param accountName Наименование счета
     * @return Возвращает айдишник владельца счета
     */
    Integer getClientByAccount(String accountName);

    /**
     * Метод возвращает айдишник счета по его наименованию
     *
     * @param accountName Наименование счета
     * @return Возвращает айдишник счета
     */
    Integer getAccount(String accountName);

    /**
     * Метод возвращает список счетов, принадлежащих одному владельцу, оборачивая все в List
     *
     * @param bankClient Владелец счета
     * @return List счетов
     */
    List<AccountClient> getAllAccountByClient(BankClient bankClient);

    /**
     * Метод закрывает счет
     *
     * @param accountName Наименование счета
     */
    void closeAccount(String accountName);

    /**
     * Метод создает новый счет
     *
     * @param clientName
     * @param accountName Наименование нового счета
     * @param balance     Изначальный баланс нового счета
     * @return Новый счет
     */
    AccountClient createNewAccount(String clientName, String accountName, Double balance);

    /**
     * Метод печатает счета клиента
     *
     * @param clientName Наименование владельца счета
     */
    void printAccount(String clientName);
}