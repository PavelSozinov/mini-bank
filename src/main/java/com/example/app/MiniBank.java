package com.example.app;

import com.example.app.models.AccountClient;
import com.example.app.models.BankClient;
import com.example.app.repositories.AccountClientRepository;
import com.example.app.repositories.BankClientRepository;
import com.example.app.service.AccountClientService;
import com.example.app.service.BankClientService;
import com.example.app.service.TransActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Scanner;

/**
 * MiniBank - основной класс для работы приложения
 */
@Controller
public abstract class MiniBank {

    @Autowired
    private AccountClientService accountClientService;

    @Autowired
    private BankClientService bankClientService;

    @Autowired
    private TransActionsService transActionsService;

    private final static String OPEN_STATUS = "Open";
    private final static String CLOSED_STATUS = "Closed";

    @Autowired
    private BankClientRepository bankClientRepository;

    @Autowired
    private AccountClientRepository accountClientRepository;

    /**
     * Метод для создания нового клиента
     *
     * @throws Exception
     */
    private void newClient() throws Exception {
        System.out.println("Введите наименование нового клиента");
        String clientName = new Scanner(System.in).nextLine();
        List<BankClient> bankClientList = bankClientRepository.findAll();
        long count = bankClientList.stream()
                .filter(accountClient -> accountClient.getName().equals(clientName))
                .count();
        if (checkName(clientName) && (count == 0)) {
            bankClientService.openBankClient(clientName);
            System.out.println("Создан новый клиент " + clientName);
        } else System.out.println("Введено некорректное наименование нового клиента");
        menuLevel1();
    }

    /**
     * Меню для работы с клиентом
     *
     * @param clientName Наименование клиента, над которым будут выполняться действия
     * @throws Exception
     */
    private void menuClientByName(String clientName) throws Exception {
        System.out.println("Выберите дальшейшее действие:");
        System.out.println("1 - Узнать статус клиента");
        System.out.println("2 - Посмотреть счета клиента");
        System.out.println("3 - Закрыть клиента (все счета также будут закрыты)");
        System.out.println("4 - Открыть клиента (счета, если они есть, остаются закрытыми)");
        System.out.println("5 - Выход в основное меню");
        String s = new Scanner(System.in).nextLine();
        int x = 0;
        try {
            x = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            System.out.println("Неверный ввод");
            menuClientByName(clientName);
        }
        while (!"5".equals(s)) {
            switch (x) {
                case 1:
                    System.out.println("Статус клиента равен " + bankClientService.checkClient(clientName));
                    menuClientByName(clientName);
                    break;
                case 2:
                    List<AccountClient> accountThisClientList = accountClientService.getAllAccountByClient(bankClientRepository.getOne(bankClientService.getClientId(clientName)));
                    if (!accountThisClientList.isEmpty()) {
                        accountClientService.printAccount(clientName);
                    } else System.out.println("У клиента нет счетов!!");
                    menuClientByName(clientName);
                    break;
                case 3:
                    if (bankClientService.checkClient(clientName).equals(OPEN_STATUS)) {
                        bankClientService.closeBankClient(clientName);
                    } else System.out.println("Клиент уже закрыт");
                    menuClientByName(clientName);
                    break;
                case 4:
                    if (bankClientService.checkClient(clientName).equals(CLOSED_STATUS)) {
                        bankClientService.openBankClient(clientName);
                        System.out.println("Клиент " + clientName + " открыт!!");
                    } else System.out.println("Клиент " + clientName + " открыт!");
                    menuClientByName(clientName);
                    break;
                case 5:
                    menuLevel1();
                    break;
                default:
                    System.out.println("Введено некорректное значение");
                    menuClientByName(clientName);
                    break;
            }
        }
        menuLevel1();
    }

    /**
     * Метод для создания новой транзакции
     *
     * @throws Exception
     */
    private void newTransActions() throws Exception {
        System.out.println("Введите номер счета кредита");
        String accountCreditName = new Scanner(System.in).nextLine();
        List<AccountClient> accountClientList = accountClientRepository.findAll();
        if (accountClientList.stream().filter((n) -> n.getStatus().equals(OPEN_STATUS)).anyMatch((n) -> n.getName().equals(accountCreditName))) {
            System.out.println("Введите номер счета дебита");
            String accountDebetName = new Scanner(System.in).nextLine();
            if (accountClientList.stream().filter((n) -> n.getStatus().equals(OPEN_STATUS)).anyMatch((n) -> n.getName().equals(accountDebetName))) {
                System.out.println("Введите сумму");
                String summaTrans = new Scanner(System.in).nextLine();
                Double summa;
                try {
                    summa = Double.parseDouble(summaTrans);
                    if (summa <= accountClientService.getAccountBalanceByName(accountCreditName)) {
                        transActionsService.createTransActions(accountDebetName, accountCreditName, summa);
                        System.out.println("Создана новая транзакция на сумму " + summa + " переводом средств от "
                                + bankClientRepository.getOne(accountClientService.getClientByAccount(accountCreditName)).getName() + " к "
                                + bankClientRepository.getOne(accountClientService.getClientByAccount(accountDebetName)).getName());
                    } else System.out.println("Недостаточно средств");
                } catch (NumberFormatException e) {
                    System.out.println("Введен некорректный баланс");
                }
            } else System.out.println("Счет не найден! или закрыт!!");
        } else System.out.println("Счет с данным именем не найден или закрыт");
        menuTransActions();
    }

    /**
     * Метод для создания нового клиента
     *
     * @param clientName Наименование клиента, которому будет открыт счет
     * @throws Exception
     */
    private void menuNewAccountClient(String clientName) throws Exception {
        System.out.println("Введите номер нового счета (максимум 5 цифр или букв)");
        String accountName = new Scanner(System.in).nextLine();
        List<AccountClient> accountClientList = accountClientRepository.findAll();
        long count = accountClientList.stream()
                .filter(accountClient -> accountClient.getName().equals(accountName))
                .count();
        if ((checkName(accountName)) && (accountName.length() == 5) && (count == 0)) {
            System.out.println("Введите исходный баланс счета");
            String startBalance = new Scanner(System.in).nextLine();
            Double balance;
            try {
                balance = Double.parseDouble(startBalance);
                accountClientService.createNewAccount(clientName, accountName, balance);
                System.out.println("Создан новый счет " + accountName);
            } catch (NumberFormatException e) {
                System.out.println("Введен некорректный баланс");
            }
        } else System.out.println("Введено некорректное наименование или такой счет уже существует!");
        menuAccount();
    }

    /**
     * Меню верхнего уровня
     *
     * @throws Exception
     */
    public void menuLevel1() throws Exception {
        int x = 0;
        String s = "";
        Scanner scan = new Scanner(System.in);
        while (!"4".equals(s)) {
            System.out.println("Выберите дальнейшее действие:");
            System.out.println("1. Меню клиента");
            System.out.println("2. Меню счетов");
            System.out.println("3. Меню транзакций");
            System.out.println("4. Выход из приложения");
            s = scan.next();
            try {
                x = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
                menuLevel1();
            }
            switch (x) {
                case 1:
                    menuClient();
                    break;
                case 2:
                    menuAccount();
                    break;
                case 3:
                    menuTransActions();
                    break;
                case 4:
                    System.out.println("Выходим");
                    break;
                default:
                    System.out.println("Введено некорректное значение!!!");
                    menuLevel1();
                    break;
            }
        }
        System.exit(0);
    }

    /**
     * Меню для работы со счетами
     *
     * @throws Exception
     */
    private void menuAccount() throws Exception {
        int x = 0;
        String s = "";
        Scanner scan = new Scanner(System.in);
        while (!"3".equals(s)) {
            System.out.println("Выберите дальнейшее действие:");
            System.out.println("1. Создать счет");
            System.out.println("2. Закрыть счет");
            System.out.println("3. Выход из текущего меню");
            s = scan.next();
            try {
                x = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
                menuAccount();
            }
            switch (x) {
                case 1:
                    System.out.println("Введите наименование клиента");
                    String clientName = scan.next();
                    List<BankClient> bankClientList = bankClientRepository.findAll();
                    if (bankClientList.stream().filter((n) -> n.getStatus().equals(OPEN_STATUS))
                            .anyMatch((n) -> n.getName().equals(clientName))) {
                        menuNewAccountClient(clientName);
                    } else System.out.println("Клиент с таким наименованием не найден!!");
                    break;
                case 2:
                    System.out.println("Введите наименование счета клиента");
                    String accountName = scan.next();
                    List<AccountClient> accountClientList = accountClientRepository.findAll();
                    if (accountClientList.stream().filter((n) -> n.getStatus().equals(OPEN_STATUS)).anyMatch((n) -> n.getName().equals(accountName))) {
                        accountClientService.closeAccount(accountName);
                    } else System.out.println("Счет с таким наименованием не найден или закрыт!");
                    menuAccount();
                    break;
                case 3:
                    System.out.println("Выходим");
                    break;
                default:
                    System.out.println("Введено некорректное значение!!!");
                    menuAccount();
                    break;
            }
        }
        menuLevel1();
    }

    /**
     * Меню для работы с клиентами
     *
     * @throws Exception
     */
    private void menuClient() throws Exception {
        int x = 0;
        String s = "";
        Scanner scan = new Scanner(System.in);
        while (!"4".equals(s)) {
            System.out.println("Выберите дальнейшее действие:");
            System.out.println("1. Создать клиента");
            System.out.println("2. Выбрать клиента");
            System.out.println("3. Посмотреть клиентов");
            System.out.println("4. Выход из текущего меню");
            s = scan.next();
            try {
                x = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
                menuClient();
            }
            switch (x) {
                case 1:
                    newClient();
                    break;
                case 2:
                    System.out.println("Введите наименование клиента");
                    String clientName = scan.next();
                    List<BankClient> bankClientList = bankClientRepository.findAll();
                    if (bankClientList.stream()
                            .anyMatch((n) -> n.getName().equals(clientName))) {
                        menuClientByName(clientName);
                    } else System.out.println("Клиент с таким наименованием не найден или закрыт!!");
                    menuClient();
                    break;
                case 3:
                    bankClientService.printClient();
                    menuClient();
                    break;
                case 4:
                    System.out.println("Выходим");
                    break;
                default:
                    System.out.println("Введено некорректное значение!!!");
                    menuClient();
                    break;
            }
        }
        menuLevel1();
    }

    /**
     * Меню для работы с транзакциями
     *
     * @throws Exception
     */
    private void menuTransActions() throws Exception {
        int x = 0;
        String s = "";
        Scanner scan = new Scanner(System.in);
        while (!"3".equals(s)) {
            System.out.println("Выберите дальнейшее действие:");
            System.out.println("1. Создать транзакцию");
            System.out.println("2. Посмотреть транзакции");
            System.out.println("3. Выход из текущего меню");
            s = scan.next();
            try {
                x = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
                menuTransActions();
            }
            switch (x) {
                case 1:
                    newTransActions();
                    break;
                case 2:
                    System.out.println("Введите наименование счета клиента, по которому вы хотите посмотреть транзакции");
                    String accountName = new Scanner(System.in).nextLine();
                    List<AccountClient> accountClientList = accountClientRepository.findAll();
                    if (accountClientList.stream().anyMatch((n) -> n.getName().equals(accountName))) {
                        transActionsService.printTransActions(accountName);
                    } else System.out.println("Счет не найден!!!");
                    menuTransActions();
                    break;
                case 3:
                    System.out.println("Выходим");
                    break;
                default:
                    System.out.println("Введено некорректное значение!!!");
                    menuTransActions();
                    break;
            }
        }
        menuLevel1();
    }

    /**
     * Проверка корректности ввода наименования клиента или счета
     *
     * @param name Наименование клиента или счета
     * @return
     */
    private boolean checkName(String name) {
        if ((name.matches("(?i).*[a-zа-я].*") || name.matches("[^0-9a-zA-Zа-яА-Я]+"))) return true;
        else return false;
    }

    public abstract void run(String... args) throws Exception;
}