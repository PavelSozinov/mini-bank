package com.example.app.models;

import javax.persistence.*;

/**
 * Класс для описания клиентов банка
 */
@Entity(name = "clients")
@Table(name = "clients")
public class BankClient {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    private String status;

    /**
     * Геттер для айдишника клиента
     *
     * @return Возвращает айдишник клиента
     */
    public Integer getId() {
        return id;
    }

    /**
     * Сеттер для айдишника клиента
     *
     * @param id Айдишник клиента
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Констуктор создания клиента
     */
    public BankClient() {
    }

    /**
     * Геттер для статуса клиента
     *
     * @return Возвращает статус клиента
     */
    public String getStatus() {
        return status;
    }

    /**
     * Сеттер для статуса клиента
     *
     * @param status Статус клиента
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Геттер для наименования клиента
     *
     * @return Возвращает наименование клиента
     */
    public String getName() {
        return name;
    }

    /**
     * Сеттер для наименования клиента
     *
     * @param name Наименование клиента
     */
    public void setName(String name) {
        this.name = name;
    }
}
