package com.example.app.models;

import javax.persistence.*;

/**
 * Класс для работы со счетами клиентов банка AccountClient со свойствами:
 * id - уникальный идентификатор счета клиента
 * name - наименование счета клиента
 * status - состояние счета (Открыт или Закрыт)
 * balance - остаток счета
 * idClient - код клиента-владельца счета
 */
@Entity(name = "accounts")
@Table(name = "accounts")
public class AccountClient {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "accountname")
    private String name;

    @Column(name = "status")
    private String status;

    @Column(name = "balance")
    private Double balance;

    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.MERGE,
                    CascadeType.REFRESH
            }, targetEntity = BankClient.class)
    @JoinColumn(name = "id_client")
    private BankClient bankClient;

    /**
     * Геттер для айдишника счета
     *
     * @return Возвращает код клиента
     */
    public Integer getId() {
        return id;
    }

    /**
     * Сеттер для айдишника счета
     *
     * @param id код счета
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Геттер для ссылки на владельца счета
     *
     * @return Возвращает ссылку на владельца счета
     */
    public BankClient getBankClient() {
        return bankClient;
    }

    /**
     * Сеттер для ссылки на владельца счета
     *
     * @param bankClient Владелец счета
     */
    public void setBankClient(BankClient bankClient) {
        this.bankClient = bankClient;
    }

    /**
     * Констуктор счета
     */
    public AccountClient() {
    }

    /**
     * Геттер для наименования счета
     *
     * @return Возвращает наименование счета
     */
    public String getName() {
        return name;
    }

    /**
     * Сеттер для наименования счета
     *
     * @param name Наименование счета
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Геттер для статуса счета
     *
     * @return Возвращает статус счета
     */
    public String getStatus() {
        return status;
    }

    /**
     * Сеттер для статуса счета
     *
     * @param status Статус счета
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Геттер для баланса счета
     *
     * @return Возвращает баланс счета
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * Сеттер для баланса счета
     *
     * @param balance Баланс счета
     */
    public void setBalance(Double balance) {
        this.balance = balance;
    }

}
