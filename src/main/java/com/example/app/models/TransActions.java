package com.example.app.models;

import javax.persistence.*;

/**
 * Класс транзакций со свойствами debet, credit, summa
 */
@Entity(name = "transactions")
@Table(name = "transactions")
public class TransActions {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.MERGE,
                    CascadeType.REFRESH
            }, targetEntity = AccountClient.class)
    @JoinColumn(name = "debet")
    private AccountClient debet;
    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.MERGE,
                    CascadeType.REFRESH
            }, targetEntity = AccountClient.class)
    @JoinColumn(name = "credit")
    private AccountClient credit;
    @Column
    private Double summa;

    /**
     * Конструктор для создания новой транзакции
     */
    public TransActions() {
    }

    /**
     * Геттер для счета дебета транзакции
     *
     * @return Возвращает ссылку на счет дебета
     */
    public AccountClient getDebet() {
        return debet;
    }

    /**
     * Сеттер для счета дебета транзакции
     *
     * @param debet Счет дебета транзакции
     */
    public void setDebet(AccountClient debet) {
        this.debet = debet;
    }

    /**
     * Геттер для счета кредита транзакции
     *
     * @return Возвращает ссылку на счет кредита
     */
    public AccountClient getCredit() {
        return credit;
    }

    /**
     * Сеттер для счета кредита транзакции
     *
     * @param credit Счет кредита транзакции
     */
    public void setCredit(AccountClient credit) {
        this.credit = credit;
    }

    /**
     * Геттер для суммы транзакции
     *
     * @return Возвращает сумму транзакции
     */
    public Double getSumma() {
        return summa;
    }

    /**
     * Сеттер для суммы транзакции
     *
     * @param summa Сумма транзакции
     */
    public void setSumma(Double summa) {
        this.summa = summa;
    }
}
