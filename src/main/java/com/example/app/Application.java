package com.example.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

/**
 * Application - класс для запуска приложения
 */
@SpringBootApplication
@ComponentScan
@EntityScan
@Profile("!test")
public class Application implements CommandLineRunner {

    @Autowired
    private MiniBank miniBank;

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(Application.class, args);
    }

    /**
     * Метод run для запуска основного класса приложения MiniBank
     *
     * @param args Аргумент для запуска приложения
     * @throws Exception
     * @see MiniBank()
     */
    @Override
    public void run(String... args) throws Exception {
        miniBank.menuLevel1();
    }
}