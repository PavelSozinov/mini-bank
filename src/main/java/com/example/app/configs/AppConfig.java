package com.example.app.configs;

import com.example.app.MiniBank;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Класс конфигурации для приложения
 */
@Configuration
public class AppConfig {

    /**
     * Bean для вызова основного меню приложения
     *
     * @return Возвращает основное меню
     */
    @Bean
    MiniBank menu() {
        return new MiniBank() {
            @Override
            public void run(String... args) throws Exception {
                menuLevel1();
            }
        };
    }
}
